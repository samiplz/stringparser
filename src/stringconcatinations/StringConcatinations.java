/**
 * Simple Application to fetch Text From Input Box and Convert it to Integer 
 * Samip Pokharel
 * samip2055@gmail.com
 */
package stringconcatinations;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class StringConcatinations {

    public static void main(String[] args) {
      String Height;
      String Breadth;
      JOptionPane.showMessageDialog(null, "Please enter all your values in cm");
     Height=JOptionPane.showInputDialog("Enter The HEIGHT");
     Breadth=JOptionPane.showInputDialog("Enter The Breadth");
     int area = Integer.parseInt(Height)*Integer.parseInt(Breadth); //Converting the String Value retrieved from the input box to integer values.
     JOptionPane.showMessageDialog(null, "The Area is " + area + "cmsqr");
     int ar = 30;
    System.out.println(String.valueOf(ar)); // A simple conversion for int to String value.
     
      System.exit(0); //Clearing the memory using this line 
    }
    
}
